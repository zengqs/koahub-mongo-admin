## KoaHub后台管理项目

使用handlebars作为前端模板,数据库使用mongoose访问MongoDB

#### KoaHub.js


KoaHub.js -- 基于 Koa.js 平台的 Node.js web 快速开发框架。可以直接在项目里使用 ES6/7（Generator Function, Class, Async & Await）等特性，借助 Babel 编译，可稳定运行在 Node.js 环境上。

github地址：http://github.com/koahubjs/koahub



## 后台管理项目

后台管理项目实现一般的项目开发的后台管理的基本功能，具体包括，但不限于以下功能：

- 管理员用户管理
- 一般用户管理,主要用于业务系统的用户的基础信息管理，支持微信公众号用户同步，第三方登录等
- 访问权限管理
- 图片管理
- 短信管理
- 视频管理
- 微信公众号管理
- Restful路由管理


## 目前已经实现的功能
### 业务功能
- 管理员用户管理
- 一般用户管理,实现基本功能
- 图片管理
- 系统配置参数管理
- 文章管理
- 访问权限管理,实现基本功能

### 中间件
- koahub-mongo中间件
- koahub-ucpass 短信发送中间件
- koahub-jwt Koa-jwt API 基于Token的鉴权封装的中间件
- koahub-framework 模块、插件管理授权机制中间件
- koahub-logger log4j的封装，支持koahub.logger.XXX XXX=debug,info,warn,error输出日志



#### 下载安装

```javascript
// 下载demo
git clone https://gitee.com/zengqs/koahub-mongo-admin.git
// 进入项目
cd koahub-mongo-admin
// 安装依赖
npm install
// 启动项目
npm start
```

#### 浏览器访问

```javascript
http://localhost:3000
```

## 官网
- [KoaHub.js官网](http://js.koahub.com)
- [作者个人网站](http://www.zengqs.com)
- [mongoose官网](http://mongoosejs.com/)




