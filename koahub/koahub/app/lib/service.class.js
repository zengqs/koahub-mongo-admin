module.exports = class Service {

    get curl(){
        return koahub.curl;
    }

    get logger(){
        return koahub.logger;
    }

    get common(){
        return koahub.common;
    }

}