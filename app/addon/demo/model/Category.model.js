const mongoose = koahub.mongoose;
const Schema = koahub.Schema;
const ObjectId = Schema.Types.ObjectId;
import {slugify} from 'transliteration';
// 定义表结构
let schema = new Schema({
    title: { //文件原始名称
        type: String,
        required: true
    },
    pinyin: String,
    parent: {
        type: ObjectId,
        ref: 'Category'
    },
    application: {
        type: String,
        default: 'article'
    }
}, {timestamps: {}});

// 参数User 数据库中的集合名称, 不存在会创建.
let Category = mongoose.model('DemoCategory', schema);

//插入测试数据
Category.findOne(function (err, data) {
    if (!data) {
        let topCat = new Category({title: '牛熊资讯', parent: null, pinyin: slugify('牛熊资讯')});
        topCat.save();

        (new Category({title: '牛熊快讯', parent: topCat, pinyin: slugify('牛熊快讯')})).save();
        (new Category({title: '牛熊秘技', parent: topCat, pinyin: slugify('牛熊秘技')})).save();
    }
});

module.exports = Category;