## 介绍

KoaHub MongoDB -- KoaHub.js mongoDB数据库访问中间件。

## 特性
> 使用mongoose访问MongoDB

## 安装
```js
npm install koahub-mongo --save
```

## 使用

```javascript
//创建app/middleware/mysql.middleware.js
module.exports = require('koahub-mongo');
//配置app/config/middleware.config.js
module.exports = {
    "koahub-mongo": {
        url:'mongodb://localhost/test'
    },
}
//配置app/config/default.config.js
module.exports = {
   loader: {
        "models": {
            root: 'model',
            suffix: '.model.js'
        }
    }
}
```

### 入门
```js
// app/home/index.controller.js
module.exports = class extends koahub.controller {

    async _initialize() {
        // 控制器初始化
    }

    async index() {
    	 
    	const User = this.model('auth/User');
    	const user =User.find({name:/Tom/}).exec()
        this.view(user);
    }
}
```

定义模型文件
> app/model/auth/User.model.js

```js
// app/model/auth/User.model.js
let mongoose = koahub.mongoose;
let userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    sex: String,
    area: String,
    always: Boolean,
    relationship: Array,
    mobile: String,
    phone: String,
    desc: String,
    id: String
});

// 参数User 数据库中的集合名称, 不存在会创建.
let User = mongoose.model('User', userSchema);

module.exports = User;
```

## Mongoosejs
[Mongoosejs API](http://mongoosejs.com/docs/api.html#Schema)

## KoaHub.js
[KoaHub.js框架](https://github.com/koahubjs/koahub)

## 官网
[KoaHub.js官网](http://js.koahub.com)