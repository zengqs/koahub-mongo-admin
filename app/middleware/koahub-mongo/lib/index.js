const mongoose = require('mongoose');
const model = require("./model");

module.exports =  function (options) {

    // const url = 'mongodb://localhost/test';

    /* 分页插件 */
    mongoose.plugin(require('./mongoose-paginate'));
    mongoose.plugin(require('./mongoose-helper'));
    // mongoose.plugin(require('./mongoose-hrbac'));

    /* 连接数据库 */
    mongoose.Promise = global.Promise;// require('bluebird');
    mongoose.connect(options.url);

    // 注入koahub
    koahub.mongoose = mongoose;
    koahub.Schema = mongoose.Schema;
    koahub.model = model;


    /**
     * 可以通过以下快捷方式创建和访问模型实例
    * let Administrator = koahub.models['admin/Administrator'];
    * let Administrator = this.models['admin/Administrator'];
    * let Administrator = this.model('admin/Administrator');
     */
    return async function (ctx, next) {
        ctx.model = model;
        // ctx.models = koahub.models;
        await next();
    };
};