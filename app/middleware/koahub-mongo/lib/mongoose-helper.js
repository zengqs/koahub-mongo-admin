'use strict';


/**
 * mongoose数据库操作扩展方法
 * @param schema
 */
function helper(schema) {


    /***
     * 插入或者更新现有记录
     * @param data
     * @returns {Promise<void>}
     */
    schema.statics.saveOrUpdate = async function (data) {

        if (('_id' in data)  && data._id !== "") {
            await this.findByIdAndUpdate(data._id, data);
            return await this.findById(data._id).exec();
        } else {
            const id=new koahub.mongoose.Types.ObjectId();
            if ('_id' in data) {
                delete data._id;
            }
            data._id=id;
            let obj = new this(data);
            await obj.save();
            return await this.findById(id).exec();
        }
    }
}


/**
 * @param {Schema} schema
 */
module.exports = helper;