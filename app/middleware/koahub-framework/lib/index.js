module.exports = function (options) {
    return async function (ctx, next) {
        const {module, controller, action} = koahub.common.getModuleControllerAction(ctx.path);


        //加载service层
        ctx.service = function (path, ...args) {
            // console.log(args);
            const service= new  koahub.services[path](...args);
            // koahub.logger.debug(service);
            return service;
        }


        //TODO 需要检测登录用户对控制器和方法的访问权限

        // koahub.logger.debug(`Module:${module}`);
        // koahub.logger.debug(`Controller:${controller}`);

        if (['admin', 'public', 'favicon.ico'].indexOf(module) != -1) { //不需要安装的系统模块
            await next();
        } else if (module === 'addon') {
            const addon = controller.split('/')[0];

            const Addon = koahub.model('admin/Addon');
            const entity = await Addon.findOne({name: addon, isSetup: true}).exec();
            if (entity) {
                await next();
            } else {
                koahub.logger.error(`插件${addon}没有安装`);
                ctx.throw(401, `插件${addon}没有安装`);
            }
        } else {
            const Module = koahub.model('admin/Module');
            const entity = await Module.findOne({name: module, isSetup: true}).exec();
            if (entity) {
                await next();
            } else {
                koahub.logger.error(`模块${module}没有安装`);
                ctx.throw(401, `模块${module}没有安装`);
            }
        }
    };
};