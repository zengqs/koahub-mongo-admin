module.exports = function (options) {
    // koahub.logger.debug(options);

    const jwt = require('jsonwebtoken');
    const jwtKoa = require('koa-jwt');

    //使用中间件鉴定访问权限
    koahub.koa.use(jwtKoa(options));
    // koahub.koa.use(jwtKoa(options).unless({
    //     path: [
    //         /^\/public/,
    //         /^\/admin/,
    //         /^\/api\/auth\/login/,
    //         /^\/api\/auth\/register/,
    //     ]
    // }));

    //unless() 用于设置哪些 api 是不需要通过 token 验证的。也就是我们通常说的 public api，无需登录就能访问的 api。
    //在使用 koa-jwt 后，所有的路由（除了 unless() 设置的路由除外）都会检查 Header 首部中的 token，是否存在、是否有效。
    // 只有正确之后才能正确的访问。

    return async function (ctx, next) {

        //不需要检测权限的API，如获取授权Token的login函数
        if (ctx.url.match(/^\/api\/auth/)) {
            await next();
        }else if(ctx.url.match(/^\/api/)) { //其他的API都需要认证

            const authorization = ctx.get('Authorization');
            // koahub.logger.debug(authorization);

            if (authorization === '') {
                ctx.throw(401, 'no token detected in http header \'Authorization\'');
            }
            //token的格式如下，第一个固定是Bearer
            //Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiNWE3NzE4OTI0YjRlN2YyODY5ZjQ1MGE5IiwiZXhwIjoxNTIwNzQ1NjkyLCJpYXQiOjE1MTgxNTM2OTJ9.Dim1Nqf4bOxtAIbcrXOJ-zxLnx3H2FqlhbN_Fo_7pVs
            const token = authorization.split(' ')[1];
            let tokenContent;
            try {
                tokenContent = await jwt.verify(token, options.secret);
                // koahub.logger.debug(`Token content:${tokenContent.toString()}`);
                // koahub.logger.debug(tokenContent);
            } catch (err) {
                if ('TokenExpiredError' === err.name) {
                    ctx.throw(401, 'token expired,请及时本地保存数据！');
                }
                ctx.throw(401, 'invalid token');
            }
            koahub.logger.debug('API鉴权成功');
            await next();
        }else{
            //其他的模块放行
            await next();
        }
    };
};