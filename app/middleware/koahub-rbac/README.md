# 框架管理RBAC中间件

曾青松，[qingsongzeng@163.com](maitlto://qingsongzeng@163.com)

2018年2月17日

## 参考实现

- https://en.wikipedia.org/wiki/NIST_RBAC_model
- https://github.com/yanickrochon/rbac-a
- https://www.npmjs.com/package/rbac-a
- https://github.com/yanickrochon/koa-rbac
- https://github.com/seeden/rbac

## 设计目标
集成框架，使用Mongo数据库保存用户和权限分配数据，与框架紧密结合形成完成的权限管理功能



