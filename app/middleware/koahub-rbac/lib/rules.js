// 数据格式
// const rules = {
//     "roles": {
//         "guest": {},
//         "reader": {
//             "permissions": ["read"],
//             "inherited": ["guest"]
//         },
//         "writer": {
//             "permissions": ["create"],
//             "inherited": ["reader"]
//         },
//         "editor": {
//             "permissions": ["update"],
//             "inherited": ["reader"],
//             "attributes": ["dailySchedule"]
//         },
//         "director": {
//             "permissions": ["delete"],
//             "inherited": ["reader", "editor"],
//         },
//         "admin": {
//             "permissions": ["manage"],
//             "inherited": ["director"],
//             "attributes": ["hasSuperPrivilege"]
//         }
//     },
//     "users": {
//         "john.smith": ["writer"],
//         "root": ["admin"]
//     }
// }

module.exports = async function () {

    //https://www.npmjs.com/package/rbac-a
    const roles = await koahub.model('admin/Role').find({}, function (err, roles) {
        // console.log(roles);
    }).exec();

    const users = await koahub.model('admin/Administrator').find({}, function (err, roles) {
        // console.log(roles);
    }).exec();

    let rules = {roles: {}, users: {}};
    if (roles) {
        for (let role of roles) {
            rules.roles[role._id] = {
                inherited: [role.parent],
                permissions: role.permissions || [],
                // attributes: []
            };
        }
    }

    if (users) {
        for (let user of users) {
            rules.users[user._id] = user.roles || [];
        }
    }


    return rules;
}