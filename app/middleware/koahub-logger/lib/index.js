/*
log4j中输入信息的级别有debug,info,warn,error,fatal 5个级别
他们对应的是输出信息的级别,级别越低信息输入越详细.使用debug级别的时候,info中的信息也能输出
使用info的时候,debug对应的信息显示不出来
一般在开发的时候使用debug, 开发完成后使用error
*/

module.exports = function (options) {
    const log4js = require('koa-log4');
    log4js.configure(options);
    const logger = log4js.getLogger('default');

    // const loggerKoa=require('koa-logger');
    // koahub.koa.use(loggerKoa());

    // 注入koahub
    koahub.logger = logger;

    return async function (ctx, next) {
        ctx.logger = logger; //将日志记录器注入到上下文中

        //代替koahub框架集成的koa-logger记录http请求信息
        const start = new Date();
        await next();
        const ms = new Date() - start;
        let msg = `${ctx.method} ${ctx.url} ${ctx.response.status} ${ms}ms`;

        if (ctx.method === 'GET') {
            msg = `<-- ${ctx.method} ${ctx.url} ${ctx.response.status} ${ms}ms`;
        } else if (ctx.method === 'POST') {
            msg = `--> ${ctx.method} ${ctx.url} ${ctx.response.status} ${ms}ms`;
        }

        // logger.info(ctx)
        logger.debug(msg)

    };
};