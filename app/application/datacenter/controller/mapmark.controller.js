import {slugify} from 'transliteration';
import base from "../../../application/admin/controller/base.controller";


export default class extends base {

    async _initialize() {
        await super._initialize();
    }

    async index() {
        await this.render('datacenter/mapmark/index', {
            status: [{key: '0', title: '关闭'}, {key: '1', title: '开启'}]
        });
    }

    async update() {

        const id = this.query.id;
        const status = this.query.status;
        const ids = id.split(',');

        for (let i in ids) {
            await this.model('datacenter/Mapmark').add({id: ids[i], status: status});
        }

        this.json('/datacenter/mapmark/index', '保存成功');
    }

    async table() {

        const p = this.query.page || 1;

        const post = this.post;
        const id = post.id.trim();
        const status = post.status.trim();
        const keywords = post.keywords.trim();

        //构造查询条件
        let queryParams = {};

        //正则表达式查询
        if (keywords !== "") {
            queryParams = {
                $or: [
                    {title: {$regex: keywords, $options: "$i"}},
                    {address: {$regex: keywords, $options: "$i"}},
                    // {mobile: {$regex: keywords, $options: "$i"}}
                ]
            }
        }

        //等值查询
        if (status !== "") {
            queryParams = Object.assign(queryParams, {status: status});
        }

        //id属性要单独查找,不支持正则表达式
        if (id !== "") {
            try {
                queryParams = {
                    _id: koahub.mongoose.Types.ObjectId(id)
                }
            } catch (err) {
                this.error('没找到符合条件的记录');
                return;
            }
        }


        // this.logger.debug(`search queryParams:${JSON.stringify(queryParams)}`);
        const pageSize= await this.common.config('LIST_ROWS');
        let pageEntity = await this.model('datacenter/Mapmark').findPage({
            page: p,
            pageSize: pageSize
        }, 'cover', queryParams, {}, {updatedAt: 'desc'}, function (error, results) {

        });

        // this.logger.debug(pageEntity);
        await this.render('datacenter/mapmark/table', {
            records: pageEntity.data,
            pager: this.common.ajaxPage(p, pageEntity.pagination.rowCount, pageEntity.pagination.pageSize)
        });
    }

    async disable() {
        const id = this.post.id || "";
        const ids = id.split(',');
        let Model = this.model('datacenter/Mapmark');
        for (const i of ids) {
            await Model.findByIdAndUpdate(i, {status: 0}).exec();
        }
        this.success('禁用成功');

    }

    async enable() {
        const id = this.post.id || "";
        const ids = id.split(',');
        let Model = this.model('datacenter/Mapmark');
        for (const i of ids) {
            await Model.findByIdAndUpdate(i, {status: 1}).exec();
            //this.changeModelField('datacenter/datacenter/Administrator', i, 'status', 1);
        }
        this.success('开启用户成功');
    }

    async delete() {
        const id = this.post.id || "";
        const ids = id.split(',');
        let Model = this.model('datacenter/Mapmark');
        for (const i of ids) {
            await Model.findByIdAndRemove(i).exec();
        }
        this.success('/datacenter/mapmark/index', '删除成功');
    }

    async add() {

        if (this.isPost()) {

            const mapmark = this.post;

            mapmark.pinyin = slugify(mapmark.title);

            await this.model('datacenter/Mapmark').saveOrUpdate(mapmark);

            this.json('/datacenter/mapmark/index', '保存成功');
        } else {

            if (this.query.id) {
                //查出关联的封面图片信息
                const mapmark = await this.model('datacenter/Mapmark').findOne({_id: this.query.id}).populate('cover');
                if (mapmark) {
                    await this.render('datacenter/mapmark/add', {mapmark: mapmark});
                } else {
                    this.redirect('/datacenter/mapmark/index?err=' + encodeURI('标记点不存在'));
                }
            } else {
                await this.render('datacenter/mapmark/add');
            }
        }
    }
}
