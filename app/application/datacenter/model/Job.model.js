const schema = require('./Job.schema');
const Model = koahub.mongoose.model('Job', schema);

module.exports = Model;