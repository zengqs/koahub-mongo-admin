const mongoose = koahub.mongoose;
const Schema = koahub.Schema;
const ObjectId = Schema.Types.ObjectId;

let schema = new Schema({
    title: {
        type: String,
        required: true,
        index:true,
    },
    pinyin: String,
    address:{
        latitude: Number,//经度
        longitude: Number,//纬度
    },
    address: String,
    photo: {
        type: ObjectId,
        ref: 'Picture',//关联File表的_id
    },
    tags: [String],
    status: {type: Number, default: 0},
}, {timestamps: {}, minimize: false, collection: 'Job'});

schema.statics.findByTitle = function (title, cb) {
    return this.find({name: new RegExp(title, 'i')}, cb);
};

module.exports = schema;