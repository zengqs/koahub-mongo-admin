module.exports = async function () {
    console.log('uninstall script is running!');
    const MenuModel = koahub.model('admin/Menu');

    //删除后台菜单数据
    await MenuModel.remove({module:'datacenter'}).exec();

    //删除数据集合
    koahub.mongoose.connection.collection('City').drop(function (err) {
        console.log(`collection City has been dropped`);
    });

    koahub.mongoose.connection.collection('Mapmark').drop(function (err) {
        console.log(`collection Mapmark has been dropped`);
    });
};