let mongoose = koahub.mongoose;
let Schema = koahub.Schema;
let ObjectId = koahub.Schema.ObjectId;
const Types = koahub.mongoose.Types;
// 定义表结构
let schema = new Schema({
    code: {type: String, required: true}, //权限的代码，唯一
    title: {type: String, required: true},//权限的名称：代码的中文解释
    description: String,//权限的描述性文字
    module: String //权限定义所属的模块，不同模块可以定义同名（code)的权限
}, {timestamps: {}, minimize: false, collection: 'Permission'});

// schema.statics.getModules = async function () {
//     const modules = await this.find().exec();
//     return modules;
// };

// schema.statics.createPermissionCode = function () {
//     return new Types.ObjectId();
// }
//
// schema.virtual('code').get(function () {
//     return this._id;
// });
//
// schema.virtual('code').set(function (value) {
//     this._id = value;
// });

let Permission = mongoose.model('Permission', schema);

Permission.findOne(async function (err, data) {
    if (!data) {

        Permission.findOne(async function (err, data) {
            if (!data) {

                const manageSystemPermissionId = new Types.ObjectId();
                const manageAccountPermissionId = new Types.ObjectId();

                Permission.create(
                    {
                        _id: manageSystemPermissionId,
                        code: 'manage_system',
                        title: '管理系统',
                        description: '后台管理的基本权限',
                        module: 'admin'
                    }
                );

                Permission.create(
                    {
                        _id: manageAccountPermissionId,
                        code: 'manage_account',
                        title: '管理系统账户信息',
                        description: '管理系统的用户信息，包含对用户的权限的分配和帐号的删除',
                        module: 'admin'
                    }
                );


                const Role = koahub.model('admin/Role');
                const adminRoleId = new Types.ObjectId();
                const systemRoleId = new Types.ObjectId();

                //一般的管理员必须具有管理系统的基本权限才能访问后台
                Role.create(
                    {
                        _id: adminRoleId,
                        parent: null,
                        title: '一般管理员',
                        code: 'admin',
                        permissions: [manageSystemPermissionId]
                    }
                );


                //系统超级管理元才能管理账户信息
                Role.create(
                    {
                        _id: systemRoleId,
                        parent: adminRoleId,//超级管理员自动拥有管理员的权限
                        code: 'system',
                        title: '系统超级管理员',
                        permissions: [manageAccountPermissionId]
                    }
                );

                //给admin账户授予超级管理员角色
                const Administrator = koahub.model('admin/Administrator');
                let system = await Administrator.findOne({username: 'system'}).exec()
                await Administrator.findByIdAndUpdate(system._id, {$push: {roles: systemRoleId}}).exec();

                let admin = await Administrator.findOne({username: 'admin'}).exec();
                await Administrator.findByIdAndUpdate(admin._id, {$push: {roles: adminRoleId}}).exec();

                // await Administrator.findById(admin._id,function(err,data){
                //     data.roles.push(roleId);
                //     data.save(function(err,data){
                //
                //     })
                // })

                admin = await Administrator.findOne({username: 'admin'}).exec();
                console.log(admin)
            }
        });
    }
});

module.exports = Permission;