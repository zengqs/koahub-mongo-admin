let mongoose = koahub.mongoose;
let Schema = koahub.Schema;
let ObjectId = koahub.Schema.ObjectId;
import {slugify} from 'transliteration';
// 定义表结构
let schema = new Schema({
    user: {type: ObjectId, ref: 'User'},
    name: String,
    pinyin:String,
    photo: {
        type: ObjectId,
        ref: 'Picture' //关联Picture表的_id
    },
    passport: {
        name: String, //姓名
        code: String,//证件号码
        authenticated: {type: Boolean, default: false},//是否已经实名认证
        snapA: {type: ObjectId, ref: 'Picture'},//身份证照片正面
        snapB: {type: ObjectId, ref: 'Picture'},//身份证照片背面
        issuingAuthority: String, //发证机关
        passportType: {type: String, default: '身份证'} //证件类型
    },
    remark: {
        type: String,
        default: ''
    },
    deletable: {type: Boolean, default: true},
    editable: {type: Boolean, default: true},
}, {timestamps: {}, minimize: false,collection:'UserExtraInfo'});


let UserExtraInfo = mongoose.model('UserExtraInfo', schema);

//插入测试数据
UserExtraInfo.findOne(function (err, data) {
    if (!data) {
        let user = new koahub.model('admin/User')({
            username: `user`,
            password: '21232f297a57a5a743894a0e4a801fc3',
            email: `user@test.com`,
            mobile: 18929540378,
            deletable: false,
            editable:true
        });
        user.save();

        const id=new koahub.mongoose.Types.ObjectId();
        user = new koahub.model('admin/User')({
            _id:id,
            username: `zengqs`,
            password: '21232f297a57a5a743894a0e4a801fc3',
            email: `zengqs@test.com`,
            mobile: 18928779564,
            deletable: true,
            editable:true
        });
        user.save();

        const extraInfo=new UserExtraInfo({
            user:id,
            name:'曾青松',
            pinyin:slugify('曾青松')
        });
        extraInfo.save();

        //插入测试数据
        for (let i = 0; i < 100; i = i + 1) {
            let user = new koahub.model('admin/User')({
                username: `user-${i}`,
                password: '21232f297a57a5a743894a0e4a801fc3',
                email: `user-${i}@test.com`,
                mobile: 18900000000 + i
            });
            user.save();
        }
    }
});

module.exports = UserExtraInfo;