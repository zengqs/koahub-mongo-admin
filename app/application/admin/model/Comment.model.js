const mongoose = koahub.mongoose;
const Schema = koahub.Schema;
const ObjectId = Schema.Types.ObjectId;

/**
 * 通用评论数据
 */
let schema = new Schema({
    parent: {type: ObjectId, ref: 'Comment', default: null},
    commenter: {type: Schema.Types.ObjectId, ref: 'User', default: null},
    body: String,
    application: String,
    model: {type: String, default: 'Article'}, //记录评论的模型
    rowId: {type: ObjectId},//记录评论的模型的ID
    module: String,
    status:{type:Number,default:1},
    ip: String
}, {timestamps: {}, minimize: false,collection:'Comment'});

// schema.statics.findByTitle = function (title, cb) {
//     return this.find({name: new RegExp(title, 'i')}, cb);
// };

let Comment = mongoose.model('Comment', schema);

module.exports = Comment;