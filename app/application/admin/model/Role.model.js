let mongoose = koahub.mongoose;
let Schema = koahub.Schema;
const Types = koahub.mongoose.Types;

// 定义表结构
let schema = new Schema({
    parent: {type: Schema.ObjectId, ref: 'Role'},
    code: {type: String, required: true},
    title: {type: String, required: true},
    description: String,
    permissions: [{type: Schema.ObjectId, ref: 'Permission'}]
}, {timestamps: {}, minimize: false, collection: 'Role'});


let Role = mongoose.model('Role', schema);

// Role.findOne(function (err, data) {
//     if (!data) {
//
//
//
//     }
// });


module.exports = Role;