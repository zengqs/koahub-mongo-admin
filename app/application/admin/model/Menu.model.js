let mongoose = koahub.mongoose;
let Schema = koahub.Schema;
let ObjectId = koahub.Schema.ObjectId;
// 定义表结构
let schema = new Schema({
    parent: {type: ObjectId, ref: 'Menu', default: null},
    title: {type: String, default: null},
    tip: String,
    rank: {type: Number, default: 0},
    hide: {type: Boolean, default: false},
    url: String,
    group: String,
    module: String,
    icon: String,
}, {timestamps: {}, minimize: false, collection: 'Menu'});


schema.statics.getMenuGroups = async function () {
    const groups = await this.distinct('group').exec();
    return groups;
};

schema.statics.getMenuGroup = async function (group) {
    const topGroup = await this.findOne({
        group: group,
        title: {$eq: null}
    }).exec();
    return topGroup;
};

schema.statics.getMenuByGroup = async function (group) {
    const itemsInGroup = await this.find({
        group: group,
        title: {$ne: null}
    }).sort({rank: 'asc'}).exec();
    return itemsInGroup;
};

let MenuModel = mongoose.model('Menu', schema);

MenuModel.findOne(function (err, data) {
    if (!data) {

        let id;

        id = new koahub.mongoose.Types.ObjectId();
        MenuModel.create({
            _id: id,
            parent: null,
            title: null,
            tip: '',
            rank: 0,
            hide: true,
            url: null,
            group: '系统',
            module: 'admin',
            icon: null
        });

        MenuModel.create({
            _id: new koahub.mongoose.Types.ObjectId(),
            parent: id,
            title: '配置管理',
            tip: '系统配置管理',
            rank: 0,
            hide: false,
            url: '/admin/config/index',
            group: '系统',
            module: 'admin',
            icon: null
        });

        MenuModel.create({
            _id: new koahub.mongoose.Types.ObjectId(),
            parent: id,
            title: '模块管理',
            tip: '系统模块管理',
            rank: 1,
            hide: false,
            url: '/admin/module/index',
            group: '系统',
            module: 'admin',
            icon: null
        });


        MenuModel.create({
            _id: new koahub.mongoose.Types.ObjectId(),
            parent: id,
            title: '插件管理',
            tip: '系统插件管理',
            rank: 2,
            hide: false,
            url: '/admin/addon/index',
            group: '系统',
            module: 'admin',
            icon: null
        });


        MenuModel.create({
            _id: new koahub.mongoose.Types.ObjectId(),
            parent: id,
            title: '图片管理',
            tip: '系统图片管理',
            rank: 4,
            hide: false,
            url: '/admin/picture/index',
            group: '系统',
            module: 'admin',
            icon: null
        });

        MenuModel.create({
            _id: new koahub.mongoose.Types.ObjectId(),
            parent: id,
            title: '后台菜单管理',
            tip: '后台管理系统的导航菜单维护',
            rank: 5,
            hide: false,
            url: '/admin/menu/index',
            group: '系统',
            module: 'admin',
            icon: null
        });
    }
});

module.exports = MenuModel;