import {slugify} from "transliteration";

let mongoose = koahub.mongoose;
let Schema = koahub.Schema;
const Types = koahub.mongoose.Types;

// 定义表结构
let schema = new Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    roles: [{type: Schema.ObjectId, ref: 'Role'}],
    mobile: String,
    email: String,
    status: {
        type: Number,
        default: 1
    },
    remark: {
        type: String,
        default: ''
    },
    deletable: {type: Boolean, default: true},
    editable: {type: Boolean, default: true},
}, {timestamps: {}, minimize: false, collection: 'Administrator'});


// 参数User 数据库中的集合名称, 不存在会创建.
let Administrator = mongoose.model('Administrator', schema);


//插入测试数据
Administrator.findOne(function (err, data) {
    if (!data) {
        const id = new Types.ObjectId();
        Administrator.create({
            _id: id,
            username: `admin`,
            password: '21232f297a57a5a743894a0e4a801fc3',
            email: `admin@test.com`,
            mobile: 18929540378,
            deletable: false,
            editable: true
        });

        Administrator.create({
            _id: new Types.ObjectId(),
            username: `system`,
            password: '21232f297a57a5a743894a0e4a801fc3',
            email: `system@test.com`,
            mobile: 18928779564,
            deletable: false,
            editable: true
        });

        // //插入测试数据
        // for (let i = 0; i < 100; i = i + 1) {
        //     Administrator.create({
        //         username: `admin-${i}`,
        //         password: '21232f297a57a5a743894a0e4a801fc3',
        //         email: `admin-${i}@test.com`,
        //         mobile: 18900000000 + i
        //     });
        // }
    }
});

module.exports = Administrator;