let mongoose = koahub.mongoose;
let Schema = koahub.Schema;
let ObjectId = koahub.Schema.ObjectId;
// 定义表结构
let schema = new Schema({
    key: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    value: String,
    valueType: {type: String, default: 'String'},//值的类型:枚举，字符串，数值
    group: {type: Number, default: 0},//配置分组
    extra: String,
    remark: {
        type: String,
        default: ''
    },
    status: {
        type: Number,
        default: 1
    },
    sort: {type: Number, default: 0},
    deletable: {type: Boolean, default: true},
    editable: {type: Boolean, default: true},
}, {timestamps: {}, minimize: false,collection:'Config'});

// 参数User 数据库中的集合名称, 不存在会创建.
let Model = mongoose.model('Config', schema);

//插入测试数据
Model.findOne(function (err, data) {
    if (!data) {
        Model.create({
            key: `CONFIG_GROUP_LIST`,
            title: '配置分组',
            value: '0:不分组|1:基本|2:内容|3:用户|4:系统|5:邮件',
            group: 4,
            valueType: 'Array',
            deletable: false,
            editable: false
        });
        Model.create({
            key: `LIST_ROWS`,
            title: '后台每页记录数',
            value: 10,
            valueType: 'Number',
            group: 2,
            deletable: false,
            editable: true
        });

        for (let i = 0; i < 100; i = i + 1) {
            Model.create({key: `key-${i}`, title: `title-${i}`, value: `value-${i}`});
        }
    }
});

module.exports = Model;