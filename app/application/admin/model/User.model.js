let mongoose = koahub.mongoose;
let Schema = koahub.Schema;
let ObjectId = koahub.Schema.ObjectId;
// 定义表结构
let schema = new Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    mobile: String,
    email: String,
    status: {
        type: Number,
        default: 1
    },
    remark: {
        type: String,
        default: ''
    },
    deletable: {type: Boolean, default: true},
    editable: {type: Boolean, default: true},
}, {timestamps: {}, minimize: false,collection:'User'});


schema.statics.findUserByIdAndRemove= async function(id){
    const UserExtraInfo= koahub.model('admin/UserExtraInfo');
    const userExtraInfo = await UserExtraInfo.findOne({user:id}).exec();
    if (userExtraInfo){
        userExtraInfo.remove();
    }
    await this.findByIdAndRemove(id).exec();
};

// 参数User 数据库中的集合名称, 不存在会创建.
let User = mongoose.model('User', schema);

//插入测试数据
User.findOne(function (err, data) {
    if (!data) {
        User.create({
            _id:'5a7718924b4e7f2869f450a9',
            username: `guest`,
            password: '21232f297a57a5a743894a0e4a801fc3',
            email: `guest@test.com`,
            mobile: '',
            deletable: false,
            editable:false
        });
    }
});

module.exports = User;