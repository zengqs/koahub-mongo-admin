let mongoose = koahub.mongoose;
let Schema = koahub.Schema;
let ObjectId = koahub.Schema.ObjectId;
// 定义表结构
let schema = new Schema({
    name: {type: String, required: true},
    alias: String,
    summary:String,
    version: {type: String, default: '1.0.0'},
    showNav: {type: Boolean, default: true},
    developer: {type: String, default: '青伢子'},
    website: String,
    entry: String,
    adminEntry: String,
    icon: String,
    canUninstall: {type: Boolean, default: true},
    isSetup: {type: Boolean, default: false},
    authRole: [ObjectId],
    // authRole: [{type:ObjectId, ref:'Role'}]
}, {timestamps: {}, minimize: false, collection: 'Module'});

schema.statics.getModules = async function () {
    const modules = await this.find().exec();
    return modules;
};


let Module = mongoose.model('Module', schema);

Module.findOne(function (err, data) {
    if (!data) {
        Module.create({
            'name': 'home',    //模块名
            'alias': '网站主页', //别名
            'version': '1.0.0',//版本号
            'showNav': 1,//是否显示在导航栏内？  1是，0否
            'summary': '网站主页',
            'developer': '青伢子',
            'website': 'http://www.zengqs.com',
            'entry': '/home/index/index', //前台入口
            'adminEntry': '/home/admin/index',
            'icon': 'home',
            'canUninstall': false //不能卸载
        });

        Module.create({
            'name': 'datacenter',    //模块名
            'alias': '数据中心', //别名
            'version': '1.0.0',//版本号
            'showNav': 1,//是否显示在导航栏内？  1是，0否
            'summary': '平台数据中心',
            'developer': '青伢子',
            'website': 'http://www.zengqs.com',
            'entry': '/datacenter/index/index', //前台入口
            'adminEntry': '/datacenter/mapmark/index',
            'icon': 'bug',
            'canUninstall': true
        });


        Module.create({
            'name': 'api',    //模块名
            'alias': '客户端接口程序', //别名
            'version': '1.0.0',//版本号
            'showNav': 1,//是否显示在导航栏内？  1是，0否
            'summary': '框架客户端API',
            'developer': '青伢子',
            'website': 'http://www.zengqs.com',
            'entry': '/api/auth/login', //前台入口
            // 'adminEntry': '/datacenter/mapmark/index',
            'icon': 'cogs', //支持Font Awesome字体图标，不需要fa-前缀
            'canUninstall': true
        });



        // Module.create({
        //     'name': 'mockmodule',    //模块名
        //     'alias': '不存在的模块，测试数据', //别名
        //     'version': '1.0.0',//版本号
        //     'showNav': 1,//是否显示在导航栏内？  1是，0否
        //     'summary': '测试数据',
        //     'developer': '青伢子',
        //     'website': 'http://www.zengqs.com',
        //     'entry': '', //前台入口
        //     // 'adminEntry': '/datacenter/mapmark/index',
        //     'icon': 'file', //支持Font Awesome字体图标，不需要fa-前缀
        //     'canUninstall': true
        // });
    }
});

module.exports = Module;