let mongoose = koahub.mongoose;
let Schema = koahub.Schema;
let ObjectId = koahub.Schema.ObjectId;
// 定义表结构
let schema = new Schema({
    name: {type: String, required: true},
    alias: String,
    summary: String,
    version: {type: String, default: '1.0.0'},
    showNav: {type: Boolean, default: true},
    developer: {type: String, default: '青伢子'},
    website: String,
    entry: String,
    adminEntry: String,
    icon: String,
    canUninstall: {type: Boolean, default: true},
    isSetup: {type: Boolean, default: false},
    authRole: [ObjectId],
    // authRole: [{type:ObjectId, ref:'Role'}]
}, {timestamps: {}, minimize: false, collection: 'Addon'});

let Model = mongoose.model('Addon', schema);

Model.findOne(function (err, data) {
    if (!data) {
        //
    }
});

module.exports = Model;