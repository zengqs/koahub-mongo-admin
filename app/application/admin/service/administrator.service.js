import base from "./base.service";

export default class extends base {

    constructor(adminId) {
        super();
        this._adminId = adminId;
    }

    async _initialize() {
        await super._initialize();

    }

    async getUserInfo() {
        const Administrator = koahub.model('admin/Administrator');
        const admin = await Administrator.findById(this._adminId).exec();
        // koahub.logger.debug(admin);
        return admin;
    }

};
