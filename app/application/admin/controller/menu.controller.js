import base from "./base.controller";
import md5 from "md5";

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const lodash = require('lodash');

export default class extends base {

    async _initialize() {
        await super._initialize();
    }

    async index() {

        let Model = this.model('admin/Module');

        let modules=[{
            key:'admin',
            title:'admin'
        }];
        for (let module of await Model.getModules()){
            modules.push({
                key:module['name'],
                title:module['name']
            })
        }

        await this.render('admin/menu/index', {
            modules: modules,
        });
    }

    async table() {
        const p = this.query.page || 1;

        const post = this.post;
        const id = post.id.trim();
        const module = post.module;
        const keywords = post.keywords.trim();

        // this.logger.debug(post);

        //构造查询条件
        let queryParams = {};

        //正则表达式查询
        if (keywords !== "") {
            queryParams = {
                $or: [
                    {title: {$regex: keywords, $options: "$i"}},
                    {tip: {$regex: keywords, $options: "$i"}}
                ]
            }
        }

        //等值查询
        if (module !== "") {
            queryParams = Object.assign(queryParams, {module: module});
        }

        //id属性要单独查找,不支持正则表达式
        if (id !== "") {
            try {
                queryParams = {
                    _id: koahub.mongoose.Types.ObjectId(id)
                }
            } catch (err) {
                this.error('没找到符合条件的记录');
                return;
            }
        }
        this.logger.debug(`search queryParams:${JSON.stringify(queryParams)}`);

        const pageSize = await this.common.config('LIST_ROWS');
        const pageEntity = await this.model('admin/Menu').findPage({
            page: p,
            pageSize: pageSize
        }, '', queryParams, {}, {module: 'asc', rank: 'asc'}, function (error, results) {
            // console.log(results);
        });

        await this.render('admin/menu/table', {
            records: pageEntity.data,
            pager: this.common.ajaxPage(p, pageEntity.pagination.rowCount, pageEntity.pagination.pageSize)
        });
    }

    async delete() {
        const id = this.post.id || "";
        const ids = id.split(',');
        const Model = this.model('admin/Menu');

        //先卸载模块，在从数据库中删除记录

        for (const i of ids) {

            const menu = await Model.findById(i,).exec();
            if (menu) {
                //卸载模块
                const script_path = path.join(__dirname, `/../../${menu['name']}/info/uninstall.js`);
                this.logger.debug(`menu install script file path : ${script_path}`);
                if (fs.existsSync(script_path)) {
                    const uninstall = koahub.common.requireDefault(script_path);
                    if (typeof uninstall === 'function') {
                      await uninstall();
                    }
                }
                //删除数据库记录
                await Model.findByIdAndRemove(i).exec();
            }

        }
        this.success('/admin/menu/index', '模块删除成功');
    }

    walk(dir) {

        dir = path.resolve(this.app, dir);

        const exist = fs.existsSync(dir);
        if (!exist) {
            return;
        }

        const files = fs.readdirSync(dir);
        let list = [];

        for (let file of files) {
            if (fs.statSync(path.resolve(dir, file)).isDirectory()) {
                list = list.concat(this.walk(path.resolve(dir, file)));
            } else {
                list.push(path.resolve(dir, file));
            }
        }

        return list;
    }

    async reset() {
        //遍历所有模块的信息，更新不一致的信息，必须有info/info.js文件
        const dir = path.join(__dirname, `/../..`);
        // this.logger.debug(dir);

        const exist = fs.existsSync(dir);
        if (!exist) {
            return;
        }

        const files = fs.readdirSync(dir);
        let list = []; //保存有模块配置信息的模块名称列表
        for (let menu of files) {
            if (fs.statSync(path.resolve(dir, menu)).isDirectory()) {
                //判断是否存在模块配置文件:menu_name/info/info.js
                if (fs.existsSync(path.resolve(dir, menu, 'info', 'info.js'))) {
                    list.push(menu);
                }
            }
        }

        const Menu = this.model('admin/Menu');

        //删除不在列表中的模块
        await Menu.remove({name: { $nin: list }}).exec();

        this.logger.debug(list);


        for (const menu of list) {
            //加载配置文件
            const info = koahub.common.requireDefault(path.resolve(dir, menu,'info', 'info'));
            //检查配置文件格式是否合格
            assert(lodash.isPlainObject(info), 'menu info must export as plain object.');

            let oldValue = await Menu.findOne({name: menu}).exec();
            if (oldValue) {
                //更新
                await Menu.findByIdAndUpdate(oldValue._id, info);
            } else {
                //创建
                await Menu.create(info);
            }
        }

        this.success('/admin/menu/index', '模块信息同步成功');
    }

    async add() {
        if (this.isPost()) {
            const menu = this.post;
            this.model('admin/Menu').saveOrUpdate(menu);
            this.success('/admin/menu/index', '保存成功');
        } else {
            //Edit
            const Model = this.model('admin/Menu');
            if (this.query.id) {
                const menu = await Model.findOne({_id: this.query.id}).exec();
                await this.render('admin/menu/add', {menu: menu});
            } else {
                this.error('admin/menu/index','不能手动添加模块。')
            }
        }
    }
}