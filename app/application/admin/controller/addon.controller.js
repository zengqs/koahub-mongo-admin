import base from "./base.controller";
import md5 from "md5";

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const lodash = require('lodash');

export default class extends base {

    async _initialize() {
        await super._initialize();
    }

    async index() {

        // let Model = this.model('admin/Addon');
        await this.render('admin/addon/index', {
            setupStatus: [{key: false, title: '未安装'}, {key: true, title: '已安装'}],
        });
    }

    async table() {
        const p = this.query.page || 1;

        const post = this.post;
        const id = post.id.trim();
        const isSetup = post.isSetup;
        const keywords = post.keywords.trim();

        this.logger.debug(post);

        //构造查询条件
        let queryParams = {};

        //正则表达式查询
        if (keywords !== "") {
            queryParams = {
                $or: [
                    {name: {$regex: keywords, $options: "$i"}},
                    {alias: {$regex: keywords, $options: "$i"}}
                ]
            }
        }

        //等值查询
        if (isSetup !== "") {
            queryParams = Object.assign(queryParams, {isSetup: isSetup});
        }

        //id属性要单独查找,不支持正则表达式
        if (id !== "") {
            try {
                queryParams = {
                    _id: koahub.mongoose.Types.ObjectId(id)
                }
            } catch (err) {
                this.error('没找到符合条件的记录');
                return;
            }
        }
        this.logger.debug(`search queryParams:${JSON.stringify(queryParams)}`);

        const pageSize = await this.common.config('LIST_ROWS');
        const pageEntity = await this.model('admin/Addon').findPage({
            page: p,
            pageSize: pageSize
        }, '', queryParams, {}, {isSetup: 'desc', name: 'asc'}, function (error, results) {
            // console.log(results);
        });

        await this.render('admin/addon/table', {
            records: pageEntity.data,
            pager: this.common.ajaxPage(p, pageEntity.pagination.rowCount, pageEntity.pagination.pageSize)
        });
    }

    async uninstall() {
        const id = this.post.id || "";
        const ids = id.split(',');
        const Model = this.model('admin/Addon');
        for (const i of ids) {
            const addon = await Model.findByIdAndUpdate(i, {isSetup: false}).exec();

            if (addon) {
                // this.logger.debug(addon);
                // this.logger.debug(koahub.paths);

                const script_path = path.join(__dirname, `/../../${addon['name']}/info/uninstall.js`);
                this.logger.debug(`addon install script file path : ${script_path}`);
                if (fs.existsSync(script_path)) {
                    const uninstall = koahub.common.requireDefault(script_path);
                    if (typeof uninstall === 'function') {
                        uninstall();
                    }
                }
            }
        }
        this.success('/admin/addon/index', '模块卸载成功');
    }

    async install() {
        const id = this.post.id || "";
        const ids = id.split(',');
        let Model = this.model('admin/Addon');
        for (const i of ids) {
            const addon = await Model.findByIdAndUpdate(i, {isSetup: true}).exec();

            if (addon) {
                // this.logger.debug(addon);
                // this.logger.debug(koahub.paths);

                const script_path = path.join(__dirname, `/../../${addon['name']}/info/install.js`);
                if (fs.existsSync(script_path)) {
                    this.logger.debug(`addon install script file path : ${script_path}`);
                    const install = koahub.common.requireDefault(script_path);
                    if (typeof install === 'function') {
                        install();
                    }
                }
            }

        }
        this.success('/admin/addon/index', '模块安装成功');
    }

    async delete() {
        const id = this.post.id || "";
        const ids = id.split(',');
        const Model = this.model('admin/Addon');

        //先卸载模块，在从数据库中删除记录

        for (const i of ids) {

            const addon = await Model.findById(i,).exec();
            if (addon) {
                //卸载模块
                const script_path = path.join(__dirname, `/../../${addon['name']}/info/uninstall.js`);
                this.logger.debug(`addon install script file path : ${script_path}`);
                if (fs.existsSync(script_path)) {
                    const uninstall = koahub.common.requireDefault(script_path);
                    if (typeof uninstall === 'function') {
                        uninstall();
                    }
                }
                //删除数据库记录
                await Model.findByIdAndRemove(i).exec();
            }

        }
        this.success('/admin/addon/index', '模块删除成功');
    }

    walk(dir) {

        dir = path.resolve(this.app, dir);

        const exist = fs.existsSync(dir);
        if (!exist) {
            return;
        }

        const files = fs.readdirSync(dir);
        let list = [];

        for (let file of files) {
            if (fs.statSync(path.resolve(dir, file)).isDirectory()) {
                list = list.concat(this.walk(path.resolve(dir, file)));
            } else {
                list.push(path.resolve(dir, file));
            }
        }

        return list;
    }

    async reset() {
        //遍历所有模块的信息，更新不一致的信息，必须有info/info.js文件
        const dir = path.join(__dirname, `/../../../addon`);
        this.logger.debug(dir);

        const exist = fs.existsSync(dir);
        if (!exist) {
            return;
        }

        const files = fs.readdirSync(dir);
        let list = []; //保存有模块配置信息的模块名称列表
        for (let addon of files) {
            if (fs.statSync(path.resolve(dir, addon)).isDirectory()) {
                //判断是否存在模块配置文件:addon_name/info/info.js
                if (fs.existsSync(path.resolve(dir, addon, 'info', 'info.js'))) {
                    list.push(addon);
                }
            }
        }

        const Addon = this.model('admin/Addon');

        //删除不在列表中的模块
        await Addon.remove({name: { $nin: list }}).exec();

        this.logger.debug(list);


        for (const addon of list) {
            //加载配置文件
            const info = koahub.common.requireDefault(path.resolve(dir, addon,'info', 'info'));
            //检查配置文件格式是否合格
            assert(lodash.isPlainObject(info), 'addon info must export as plain object.');

            let oldValue = await Addon.findOne({name: addon}).exec();
            if (oldValue) {
                //更新
                await Addon.findByIdAndUpdate(oldValue._id, info);
            } else {
                //创建
                await Addon.create(info);
            }
        }

        this.success('/admin/addon/index', '模块信息同步成功');
    }

    async add() {

        if (this.isPost()) {

            const addon = this.post;

            if (addon.key) {

            } else {
                this.error('参数错误')
            }

            if (addon.id === "") {
                delete addon.id;
            }

            this.model('admin/Addon').saveOrUpdate(addon);

            this.success('/admin/addon/index', '保存成功');

        } else {
            //Edit
            const Model = this.model('admin/Addon');
            const data = await Model.findOne({key: `CONFIG_GROUP_LIST`}).exec();
            let groups = [];
            for (const item of data['value'].split('|')) {
                const pairs = item.split(':');
                groups.push({key: Number(pairs[0]), title: pairs[1]});
            }

            const valueTypes = [
                {key: 'Number', title: '数字'},
                {key: 'String', title: '字符串'},
                {key: 'Text', title: '文本'},
                {key: 'Array', title: '数组'},
                {key: 'MultiCheckList', title: '多选框'}
            ];

            const options = {
                groups: groups,
                valueTypes: valueTypes
            };

            if (this.query.id) {
                const addon = await Model.findOne({_id: this.query.id}).exec();
                await this.render('admin/addon/add', Object.assign(options, {addon: addon}));
            } else {
                //add new item
                await this.render('admin/addon/add', options);
            }
        }
    }
}