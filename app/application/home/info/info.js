module.exports = {
    'name': 'home',    //模块名
    'alias': '网站主页', //别名
    'version': '1.0.0',//版本号
    'showNav': 1,//是否显示在导航栏内？  1是，0否
    'summary': '网站主页',
    'developer': '青伢子',
    'website': 'http://www.zengqs.com',
    'entry': '/home/index/index', //前台入口
    'adminEntry': '/home/admin/index',
    'icon': 'home',
    'canUninstall': false
};