module.exports = class extends koahub.controller {

    async _initialize() {
        // console.log('_initialize')
    }

    async index() {
        const version = process.version;
        const time = new Date();
        const num = this.randomCode(8);
        const id = this.query.id;
        this.logger.debug(this.ctx.params);

        await this.render('home/index/index',{'version':version,'time':time,'num':num});

    }
};