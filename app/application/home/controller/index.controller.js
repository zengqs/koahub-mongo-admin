module.exports = class extends koahub.controller {

    async _initialize() {
        console.log('_initialize')



    }

    async addon(){
        await this.action('/addon/demo/index/index');
    }

    async sendsms(){
        koahub.sendsms('87828,1','18928779564');
    }

    async index() {
        const version = process.version;
        const time = new Date();
        const num = this.randomCode(8);

        await this.render('home/index/index',{'version':version,'time':time,'num':num});

    }
};