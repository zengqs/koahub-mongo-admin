module.exports = {
    'name': 'api',    //模块名
    'alias': '客户端接口程序', //别名
    'version': '1.0.0',//版本号
    'showNav': false,//是否显示在导航栏内？  1是，0否
    'summary': '框架客户端API',
    'developer': '青伢子',
    'website': 'http://www.zengqs.com',
    'entry': '/api/auth/login', //前台入口
    // 'adminEntry': '/datacenter/mapmark/index',
    'icon': 'cogs', //支持Font Awesome字体图标，不需要fa-前缀
    'canUninstall': true
};