const base = require("./base.controller");

module.exports = class extends base {

    async _initialize() {
        await super._initialize();
    }

    //router: http://www.zengqs.com/api/user/:id
    // http://localhost:3000/api/user/detail?id=5a7718924b4e7f2869f450a9
    async detail() {
        const id = this.query.id || null;
        let User = this.model('admin/User');

        // koahub.logger.debug('User info:');
        // koahub.logger.debug(this.ctx.state.user);
        const userId = this.ctx.state.user.data;//Auth操作在设置token的时候设置了 userId值
        if (userId !== id) {
            this.ctx.throw(401, '非法访问，只能获取已经登陆的用户自己的信息。');
        }

        let admin = await User.findById(id).exec();
        if (admin) {
            //删除敏感信息
            admin['password'] = undefined;
            admin['__v'] = undefined;
            // const {_v, password, ...rest} = admin;
            this.success(admin);
        } else {
            this.ctx.throw(401, '非法访问，只能获取已经登陆的用户自己的信息。');
        }

    }
};
