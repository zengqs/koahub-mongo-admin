module.exports = {

    //middleware顺序
    middleware: ['koahub-logger', 'koahub-jwt', 'koahub-handlebars','koahub-mongo', 'koahub-framework'],

    // "koa-logger":true,

    'koahub-logger': {
        "appenders": {
            "rule-console": {
                "type": "console",
            },
            "rule-error": {
                "type": "dateFile",
                "filename": "logs/error-",
                "encoding": "utf-8",
                "maxLogSize": 1000000,
                "numBackups": 3,
                "pattern": "yyyy-MM-dd.log",
                "alwaysIncludePattern": true
            }
        },
        "categories": {
            "default": {
                "appenders": [
                    "rule-console",
                    "rule-error"
                ],
                "level": "debug"
            }
        },
    },

    'koahub-jwt': {
        secret: 'shared-secret',
        passthrough: true,
        expiresIn: '7 days'
    },

    "koahub-handlebars": {
        extname: ".html",
        viewPath: "./www",
        layoutsPath: "./www",
        partialsPath: "./www",
        disableCache: true
    },

    'koahub-framework': {

    },

    'koahub-rbac': {

    },

    'koahub-mongo': {
        url: 'mongodb://localhost/test'
    },

    'koahub-ucpass': {
        sid: '2712474cd5ebc0753909e43f968cf0ee',                       //开发者账号id
        token: 'e95abf6189133e153cb5d9b6dcb484bf',                     //开发者token
        appid: 'eff851143bf94fdf9ab8bd43c6f2abc6',                     //应用id
        templateid: '88137',                //短信模板id
        uid: ''                       //透传uid   可为空
    },

    // "koahub-wechat":{
    //     "port": '8080',
    //     "token": "xxx",
    //     "appID": "xxx",
    //     "appSecret": "xxx",
    //     "encodingAESKey": "xxx",
    //     "apiDomain": "https://api.weixin.qq.com/",
    //     "accessTokenFilePath": "./static/access_token.json",
    //     // 图灵ApiKey
    //     "tulingApiKey": "65f46644fcc945eeb0c3ef55bf75b799",
    //     // 是否开启图灵机器人：1为开启，0为关闭
    //     "tulingActive": 1,
    //     // 管理员的openid todo
    //     "administrator": []
    // },

    //favicon设置
    'koa-favicon': 'www/favicon.ico',

    //body配置
    body: {
        multipart: true
    },

    //cors配置
    'koa-cors': false,

    //session配置
    'koa-session2': {
        key: 'koahub.js'
    },

    'koa-static-cache': {
        dir: 'www',
        dynamic: true
    },
};