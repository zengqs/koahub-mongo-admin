module.exports = {

    //启动端口
    port: 3000,

    //默认模块，控制器，操作
    default_module: 'admin',
    default_controller: 'index',
    default_action: 'index',

    //url后缀
    url_suffix: '',

    //自动加载配置
    loader: {
        'middlewares': {
            root: 'middleware',
            suffix: '.middleware.js'
        },
        'controllers': [{
            root: 'application',
            suffix: '.controller.js',
            prefix: '/',
            filter: [/\/controller/]
        }, {
            root: 'addon',
            suffix: '.controller.js',
            prefix: '/addon/',
            filter: [/\/controller/]
        }],
        'models': [{
            root: 'application',
            suffix: '.model.js',
            filter: [/\/model/]
        }, {
            root: 'addon',
            suffix: '.model.js',
            filter: [/\/model/]
        }],
        "services": [{
            root: 'application',
            suffix: '.service.js',
            // prefix: '/',
            filter: [/\/service/]
        },
        //     {
        //     root: 'addon',
        //     suffix: '.service.js',
        //     prefix: '/',
        //     filter: [/\/service/]
        // }
        ],
        "configs": [{
            root: 'config',
            suffix: '.config.js'
        }, {
            root: 'application',
            suffix: '.config.js',
            filter: [/\/config/]
        }, {
            root: 'addon',
            suffix: '.config.js',
            filter: [/\/config/]
        }],
        'utils': {
            root: 'util',
            suffix: '.util.js',
        }
    }
};