const pagination= require("pagination"); //分页
// const moment = require('moment');

export function randomCode(num) {
    let code = "";
    for (let i = 0; i < num; i++) {
        code += Math.floor(Math.random() * 10);
    }
    return code;
}

//把返回的数据集转换成Tree,并按规则排序
export function list_to_tree(list, id, pid, child, sort, desc) {
    sort = sort ? sort : 'id';
    let pos = {};
    let tree = [];
    let i = 0;
    while (list.length != 0) {
        if (list[i][pid] == 0) {
            list[i][child] = [];
            tree.push(list[i]);

            pos[list[i][id]] = [tree.length - 1];
            list.splice(i, 1);
            i--;
        } else {
            let posArr = pos[list[i][pid]];
            if (posArr != undefined) {

                let obj = tree[posArr[0]];
                for (let j = 1; j < posArr.length; j++) {
                    obj = obj[child][posArr[j]];
                }

                list[i][child] = [];
                obj[child].push(list[i]);

                pos[list[i][id]] = posArr.concat([obj[child].length - 1]);
                if (desc) {
                    obj[child].sort(function (a, b) {
                        return b[sort] - a[sort]
                    });
                } else {
                    obj[child].sort(function (a, b) {
                        return a[sort] - b[sort]
                    });
                }
                list.splice(i, 1);
                i--;
            }
        }
        i++;
        if (i > list.length - 1) {
            i = 0;
        }
    }
    if (desc) {
        tree.sort(function (a, b) {
            return b[sort] - a[sort]
        });
    } else {
        tree.sort(function (a, b) {
            return a[sort] - b[sort]
        });
    }
    return tree;
}


export function ajaxPage(current, total, perPage, get_page) {
    perPage = perPage || 25;
    get_page = get_page || 'get_page';


    const page = new pagination.TemplatePaginator({
        prelink: '', current: current, rowsPerPage: perPage,
        totalResult: total,
        template: function (result) {
            let i, len;
            let html = '<div><ul class="pagination no-margin">';
            if (result.pageCount < 2) {
                html += '<li><a style="background: #fff;">共 ' + total + ' 条记录  1/1 页</a></li>';
                html += '</ul></div>';
                return html;
            }
            if (result.previous) {
                html += `<li><a href="javascript:${get_page}(${result.previous })" style="background: #fff;">上一页</a></li>`;
            }
            if (result.range.length) {
                for (i = 0, len = result.range.length; i < len; i++) {
                    if (result.range[i] === result.current) {
                        html += `<li class="active"><a href="javascript:${get_page}(${result.range[i]})" style="border-color: #dd4b39;background-color: #dd4b39;">${result.range[i]}</a></li>`;
                    } else {
                        html += `<li><a href="javascript:${get_page}(${result.range[i]})" style="background: #fff;">${result.range[i]}</a></li>`;
                    }
                }
            }
            if (result.next) {
                html += `<li><a href="javascript:${get_page}(${result.next})" class="paginator-next" style="background: #fff;">下一页</a></li>`;
            }
            html += '</ul></div>';
            return html;
        }
    });
    return page.render();
}


// /**
//  * pagination(page, file.pagination.rowCount, '/admin/file/index')
//  *
//  * @param current
//  * @param total
//  * @param prelink
//  * @param perPage
//  * @returns {*}
//  */
// export function page(current, total, prelink, perPage) {
//     perPage = perPage || 25;
//
//     const page = new pagination.TemplatePaginator({
//         prelink: prelink || '', current: current, rowsPerPage: perPage,
//         totalResult: total,
//         template: function (result) {
//             let i, len, prelink;
//             let html = '<div><ul class="pagination no-margin">';
//             if (result.pageCount < 2) {
//                 html += '<li><a style="background: #fff;">共 ' + total + ' 条记录  1/1 页</a></li>';
//                 html += '</ul></div>';
//                 return html;
//             }
//             prelink = this.preparePreLink(result.prelink);
//             if (result.previous) {
//                 html += '<li><a href="' + prelink + result.previous + '" style="background: #fff;">上一页</a></li>';
//             }
//             if (result.range.length) {
//                 for (i = 0, len = result.range.length; i < len; i++) {
//                     if (result.range[i] === result.current) {
//                         html += '<li class="active"><a href="' + prelink + result.range[i] + '" style="border-color: #dd4b39;background-color: #dd4b39;">' + result.range[i] + '</a></li>';
//                     } else {
//                         html += '<li><a href="' + prelink + result.range[i] + '" style="background: #fff;">' + result.range[i] + '</a></li>';
//                     }
//                 }
//             }
//             if (result.next) {
//                 html += '<li><a href="' + prelink + result.next + '" class="paginator-next" style="background: #fff;">下一页</a></li>';
//             }
//             html += '</ul></div>';
//             return html;
//         }
//     });
//
//     return page.render();
// }

/**
 * 设置或者读取系统配置项目的值
 * @param key
 * @param value
 * @returns {Promise<void>}
 * @constructor
 */
export async function config(key, value) {

    //需要增加缓存提高性能
    if (typeof key === "undefined") {
        koahub.logger.error(`必须指定key属性`);
        throw new Error(`必须指定key属性`);
    }

    const Config = koahub.model('admin/Config');

    if (typeof value === "undefined") {
        const row = await Config.findOne({key: key.toUpperCase()}).exec();
        if (row) {
            koahub.logger.debug(`C[${key}]= ${row.value}`);
            return row.value;
        } else {
            koahub.logger.error(`Key : ${key} 的配置项目不存在`);
            throw new Error(`Key : ${key} 的配置项目不存在`);
        }

    } else {
        const row = await Config.findOne({key: key.toUpperCase()}).exec();
        if (row) { //更新
            if (row.editable) {
                row.value = value;
                await Config.saveOrUpdate(row);
            } else {
                koahub.logger.error(`Key : ${key} 的配置项目不允许更改`);
                throw new Error(`Key : ${key} 的配置项目不允许更改`);
            }
        } else { //add new item
            await Config.saveOrUpdate({key: key, value: value});
        }
    }


}

// export async function uploadImage(files) {
//     const Picture = koahub.model('admin/Picture');
//
// }